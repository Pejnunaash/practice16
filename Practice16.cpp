﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int Day = buf.tm_mday;


    const int N = 4;
   int Array[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            Array[i][j] = i + j;
            std::cout << Array[i][j];
        }
        std::cout << '\n';
    }

    int ArrayIndex = Day % N;
    std::cout << "Nomer stroki:" << ArrayIndex << '\n';
    
    int Summ = 0;

    for (int i = 0; i < N; i++) {
        Summ = Summ + Array[ArrayIndex][i];
    }
    std::cout << "Summa elementov stroki:" << Summ;
}